
Features:

* create PDF-pages from text or pictures
* add PDF-pages from text or pictures to existing PDFs
* merge PDFs
* protect PDFs with password
* delete pages from existing PDFs
* edit metadata from existing PDFs
* convert PDFs to images
.


