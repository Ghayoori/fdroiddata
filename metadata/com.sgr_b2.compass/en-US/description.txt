
An outdoors application which can help you to find your way though the forest or
desert, or whatever. It provides inter-device location exchange by SMS and can
make distress call even if the device screen is broken by sending SMS with your
location on your behalf.

Due to pointer-navigation nature, it can also be used as either magnetic or GPS
compass reading direction to north from your device magnetic sensors or from GPS
chip.
.


