Categories:System
License:Apache-2.0
Author Name:TheRedSpy15
Web Site:
Source Code:https://github.com/TheRedSpy15/LTECleanerFOSS
Issue Tracker:https://github.com/TheRedSpy15/LTECleanerFOSS/issues
Changelog:https://github.com/TheRedSpy15/LTECleanerFOSS/releases

Auto Name:LTE Cleaner (FOSS)
Summary:System cleaner
Description:
Tired of the abundance of phone cleaners on the play store? Tired of them being
extremely shady? Tired of them doing nothing? Tired of ads? Tired of having to
pay? Me too.

Android API 23+ (Marshmallow) no longer allows apps to clean other app's caches,
thus the claim of cleaners clearing them is completely false.

The only thing they can clean: log files, temporary files, and empty folders.
All three ''LTE Cleaner'' wipes.

''LTE Cleaner'' is %100 free, open source, ad free, and deletes everything it
claims too.
.

Repo Type:git
Repo:https://github.com/TheRedSpy15/LTECleanerFOSS.git

Build:2.0,7
    commit=2.0
    subdir=app
    gradle=yes

Build:2.3,10
    commit=2.3
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.3
Current Version Code:10
