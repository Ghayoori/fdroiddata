
Blocks messages from senders not in your contact list, filtering spam.

Features include:

* Silent notifications and storage of messages from unknown senders
* Add sender to system address book
* Delete message or move to Inbox
.


