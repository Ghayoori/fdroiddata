
DAVdroid is a CalDAV/CardDAV management and synchronization app for Android.
which integrates natively with Android calendar/contact apps.

Use it with your own server or with a trusted hoster to keep your contacts,
events and tasks under your control.

For a comparison of server software, see the [https://wiki.debian.org/Groupware
Debian wiki]. Examples (alphabetical order): [http://baikal-server.com/ Baïkal],
[http://www.davical.org/ DAViCal], [https://nextcloud.com/ Nextcloud],
[https://owncloud.org/ ownCloud], [http://radicale.org/ Radicale])
.


