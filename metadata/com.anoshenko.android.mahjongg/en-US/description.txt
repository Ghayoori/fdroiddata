
A collection of 19 solitaire games where the object is to remove all pieces from
the game board by finding matching pairs of images from the both ends of lines
of pieces.

The built-in layout editor allows creation of new game layouts.
.
