
Simple PDF reader based on [[cx.hell.android.pdfview]].

Features:

* Textual search support
* Flexible key/button/gesture configuration
* Different color schemes for comfortable reading
* Optimizations for eInk
* x86 support
* Pinch to zoom
* Bookmarks
.


