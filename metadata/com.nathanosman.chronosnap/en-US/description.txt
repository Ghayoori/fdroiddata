
Capture a sequence of photos at regular intervals.

Features:

* Customize the capture interval
* Set a limit on the number of images captured
* Capture images with the screen turned off
* Persistent notification during capture
* Interface uses material design guidelines
.


