
Prevents your smartphone or tablet from leaking privacy sensitive information
via Wi-Fi networks. It does this in two ways:

* It prevents your smartphone from sending out the names of Wi-Fi networks it wants to connect to over the air. This makes sure that other people in your surroundings can not see the networks you’ve connecte to, and the places you’ve visited.
* If your smartphone encounters an unknown access point with a known name (for example, a malicious access point pretending to be your home network), it asks whether you trust this access point before connecting. This makes sure that other people are not able to steal your data.

Further details are discussed in two papers, a
[https://brambonne.com/docs/bonne14sasquatchprivacypolice.pdf short one] and
[https://brambonne.com/docs/bonne14sasquatch.pdf long version].
.


