
An Android front end for the Nextcloud/Owncloud Bookmark App based on the new
REST API that was introduced by Bookmarks version 0.10.2 With this app you can
add/edit/delete and view bookmarks, and sync them with your ownCloud.

However you need to have the Bookmarks app in minimal required version 0.10.2
installed and enabled on you ownCloud.

If you need more information about the Nextcloud Bookmark app, you can follow
this link: [https://marketplace.owncloud.com/apps/bookmarks]
.


