Categories:Writing
License:MIT
Web Site:
Source Code:https://github.com/rubenroy/Minimal-Todo
Issue Tracker:

Auto Name:Minimal
Summary:Minimal TODO lists

Repo Type:git
Repo:https://github.com/rubenroy/Minimal-Todo.git

Build:1.2,3
    commit=7cd0fa0de23fe5f0058cfb50d530e81da7b3eebb
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest/fdroid
Current Version:1.2
Current Version Code:3
