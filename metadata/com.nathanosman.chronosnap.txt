Categories:Multimedia,Graphics
License:MIT
Web Site:https://github.com/nathan-osman/chronosnap/blob/HEAD/README.md
Source Code:https://github.com/nathan-osman/chronosnap
Issue Tracker:https://github.com/nathan-osman/chronosnap/issues

Auto Name:ChronoSnap
Summary:Take photos in intervals

Repo Type:git
Repo:https://github.com/nathan-osman/chronosnap

Build:1.0.4,5
    commit=v1.0.4
    subdir=app
    gradle=yes

Build:1.0.5,6
    commit=v1.0.5
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.5
Current Version Code:6
