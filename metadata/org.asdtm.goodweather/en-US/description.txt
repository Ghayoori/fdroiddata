
Show current weather information from [http://openweathermap.org/
OpenWeatherMap].

Support of Basque, Belarusian, Czech, English, French, German, Japanese,
Spanish, Polish, Russian languages.

Features:

* Current weather
* 7 day forecast
* 7 day forecast graphs
* Many locations
* Notifications
* Support different measuring units
* Ad-free
.


